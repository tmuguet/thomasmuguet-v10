---
title: "Miroir des Fétoules"
date: 2022-11-20T00:00:00+00:00
images: ["images/IMGP1355.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Automne"
types: "Marche"
---

Balade familiale au dessus de Saint Christophe en Oisans et de la vallée du Vénéon.
