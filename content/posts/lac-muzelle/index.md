---
title: "Lac de la Muzelle"
date: 2018-10-21T00:00:00+00:00
images: ["images/IMGP5460.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: "Automne"
types: "Marche"
seealso: ["posts/lacs-muzelle-lauvitel"]
tags: ["Lac"]
---

Randonnée irréelle pour les derniers beaux jours d'automne.
