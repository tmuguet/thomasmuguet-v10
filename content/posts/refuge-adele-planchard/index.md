---
title: "Refuge Adèle Planchard"
date: 2016-08-28T00:00:00+00:00
images: ["images/IMGP3079.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Panorama", "3000"]
---

Randonnée assez longue (beaucoup de dénivelé, arrivée au dessus de 3000m), plus facile avec une nuit au refuge.
