---
title: "Mont Rachais"
date: 2021-04-15T00:00:00+00:00
images: ["images/IMGP7414.jpg"]
massifs: "Chartreuse"
saisons: "Printemps"
types: "Marche"
---

Petite randonnée classique, au départ du col de Vence.
