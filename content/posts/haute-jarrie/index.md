---
title: "Haute-Jarrie"
date: 2021-04-25T00:00:00+00:00
images: ["images/IMGP7714.jpg"]
massifs: "Belledonne"
saisons: "Printemps"
types: "Marche"
---

Ballade autour de Grenoble
