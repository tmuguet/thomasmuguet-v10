---
title: "Sous le Moucherotte"
date: 2021-06-17T00:00:00+00:00
images: ["images/IMGP7449.jpg"]
massifs: "Vercors"
saisons: "Printemps"
types: "Marche"
tags: ["Panorama"]
---

Randonnée sous le Moucherotte. En début de printemps, avortée à cause de la neige trop abondante; en fin de printemps avec une montée sous les Trois Pucelles.
