---
title: "Bec de l'Orient, Pas de la Clé et Grande Brèche"
date: 2018-10-14T00:00:00+00:00
images: ["images/IMGP5305.jpg"]
massifs: "Vercors"
saisons: "Automne"
types: "Marche"
seealso: "posts/pas-cle"
tags: ["Panorama"]
---

Randonnée sur les balcons du plateau de Gève. Chemin parfois proche de la falaise, mais qui ne donne pas le vertige.
