---
title: "Grande Sure"
date: 2016-07-10T00:00:00+00:00
images: ["images/IMGP2581.jpg"]
massifs: "Chartreuse"
saisons: "Été"
types: "Marche"
tags: ["Panorama"]
---

Randonnée estivale avec un joli panorama
