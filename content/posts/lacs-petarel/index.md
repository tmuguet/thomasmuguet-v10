---
title: "Lacs de Pétarel"
date: 2015-10-31T00:00:00+00:00
images: ["images/IMGP1426.jpg"]
massifs: ["Écrins", "Valgaudémar"]
saisons: "Automne"
types: "Marche"
tags: ["Lac"]
---

Au coeur des Écrins aux premières neiges
