---
title: "Refuge des sources"
date: 2021-09-06T00:00:00+00:00
images: ["images/IMGP9247.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Framboises", "Myrtilles", "Marmottes"]
---

Randonnée peu difficile, mais avec un passage délicat (un peu de gaz, dans un pierrier) au début.

La route d'accès à Villard-Notre-Dame depuis Bourg d'Oisans est une vraie route de montagne (croisement difficile/impossible sur 8km), fortement déconseillée pour ceux qui n'ont pas l'habitude.
