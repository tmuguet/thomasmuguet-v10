---
title: "Molière"
date: 2016-03-19T00:00:00+00:00
images: ["images/IMGP1887.jpg"]
massifs: "Vercors"
saisons: "Hiver"
types: "Raquettes"
---

Randonnée en raquettes tranquille entre Saint-Nizier-du-Moucherotte et Autrans
