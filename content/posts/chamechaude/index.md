---
title: "Chamechaude"
date: 2023-06-18T00:00:00+00:00
images: ["images/IMGP0880.jpg"]
massifs: "Chartreuse"
saisons: "Été"
types: "Marche"
tags: ["Panorama"]
---

Point culminant de la Chartreuse; arrivée un peu délicates (câbles) et venteuse !
