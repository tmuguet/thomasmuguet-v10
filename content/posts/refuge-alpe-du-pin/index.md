---
title: "Refuge de l'Alpe du Pin et vallon de la Mariande"
date: 2023-07-01T00:00:00+00:00
images: ["images/IMGP1455.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: ["Automne", "Été"]
types: "Marche"
---

Boucle tranquille dans les Écrins à l'Alpe du Pin. Possibilité de pousser au vallon de la Mariande (en aller-retour) pour une journée plus sportive.
