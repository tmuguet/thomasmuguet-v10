---
title: "Lac Fourchu"
date: 2022-06-12T00:00:00+00:00
images: ["images/IMG_20200530_210502.jpg"]
massifs: "Belledonne"
saisons: "Printemps"
types: "Marche"
tags: ["Lac"]
---

Bivouac au lac Fourchu en 2020, et randonnée à la journée au plateau des lacs et retour via les châlets de la Barrière en 2022.
