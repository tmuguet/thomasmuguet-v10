---
title: "Vallon de la Selle"
date: 2019-06-23T00:00:00+00:00
images: ["images/IMGP6412.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
---

Montée en aller/retour au refuge, sous la glacier de la Selle, facile sur la journée.
