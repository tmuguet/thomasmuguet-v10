---
title: "Malaterre"
date: 2018-04-07T00:00:00+00:00
images: ["images/IMGP4360.jpg"]
massifs: "Vercors"
saisons: "Printemps"
types: "Marche"
---

Balade dans la forêt de la Loubière sur les hauteurs de Villard-de-Lans.
