---
title: "Chaos de Bellefond"
date: 2019-11-24T00:00:00+00:00
images: ["images/IMG_20191124_130416.jpg"]
massifs: "Chartreuse"
saisons: "Automne"
types: "Marche"
---

Balade dans les premières neiges.
