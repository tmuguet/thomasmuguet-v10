---
title: "Lac Lovitel et fontaine pétrifiante"
date: 2021-06-12T00:00:00+00:00
images: ["images/IMGP8244.jpg"]
massifs: ["Cerces", "Oisans", "Écrins"]
saisons: "Printemps"
types: "Marche"
tags: ["Cascade", "Tourbière"]
---

Ballade parfois très aérienne surplombant le lac du Chambon. On poussera jusqu'à la fontaine prétrifiante, pour mériter l'arrêt au refuge des Clots.
