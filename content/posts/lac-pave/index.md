---
title: "Lac du Pavé"
date: 2017-08-27T00:00:00+00:00
images: ["images/IMGP4001.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: ["Été"]
types: "Marche"
tags: ["Lac"]
---

Randonnée vers le plus haut lac du parc des Écrins, avec nuit en refuge.
