#!/bin/bash
rsync public/ ubuntu@baz.thomasmuguet.info:/var/www/gallery/ -a -v --delete --delete-excluded --exclude='.DS_Store' --progress --dry-run

read -r -p "Continue? [y/N] " input
case $input in
    [yY][eE][sS]|[yY])
        rsync public/ ubuntu@baz.thomasmuguet.info:/var/www/gallery/ -a -v --delete --delete-excluded --exclude='.DS_Store' --progress
        ;;
    *)
        exit 1
        ;;
esac
