---
title: "Croix de Chamrousse"
date: 2011-11-12T00:00:00+00:00
images: ["images/DSCF0578.jpg"]
massifs: "Belledonne"
saisons: "Automne"
types: "Marche"
tags: ["Panorama"]
---

Randonnée automnale avant les premières neiges...
