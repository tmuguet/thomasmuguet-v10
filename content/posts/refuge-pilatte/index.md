---
title: "Refuge de la Pilatte"
date: 2016-08-15T00:00:00+00:00
images: ["images/IMGP2780-Modifier.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Panorama"]
---

Randonnée un peu longue mais peu difficile, avec nuit au pied du glacier de la Pilatte. Possibilité de faire l'aller/retour sur une (longue) journée.
