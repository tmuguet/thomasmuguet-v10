---
title: "Charmant Som"
date: 2020-07-19T00:00:00+00:00
images: ["images/IMGP5202.jpg"]
massifs: "Chartreuse"
saisons: ["Automne", "Printemps", "Été"]
types: "Marche"
seealso: ["posts/pinea-charmant-som"]
tags: ["Panorama"]
---

Balade tranquille, montée par un chemin méconnu -un peu plus exigeant- pour éviter l'autoroute
