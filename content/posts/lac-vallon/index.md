---
title: "Lac du Vallon"
date: 2018-07-28T00:00:00+00:00
images: ["images/IMGP4759.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Lac"]
---

Magnifique lac d'altitude en bordure du parc national des Écrins
