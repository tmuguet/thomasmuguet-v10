---
title: "Pic Saint Michel"
date: 2015-10-24T00:00:00+00:00
images: ["images/IMGP1295.jpg"]
massifs: "Vercors"
saisons: ["Automne", "Hiver"]
types: ["Marche", "Raquettes"]
tags: ["Panorama"]
---

Vue à 360° sur Grenoble et le plateau du Vercors.
