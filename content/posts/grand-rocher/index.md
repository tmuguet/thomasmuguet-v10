---
title: "Grand Rocher"
date: 2017-10-15T00:00:00+00:00
images: ["images/IMGP4086-4090.jpg"]
massifs: "Belledonne"
saisons: ["Automne", "Hiver"]
types: ["Marche", "Raquettes"]
tags: ["Panorama"]
---

Point de vue à 360° sur Belledonne et Vercors.
