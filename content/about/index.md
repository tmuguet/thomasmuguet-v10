---
title: "A propos"
images: ["images/DSCF0348.jpg"]
layout: "plain"
_build:
    publishResources: false
---

## Site de randonnées/photos autour de Grenoble

Les randonnées listées ici ne doivent pas servir de topo (d'autres sites le font mieux que moi), mais pourront servir d'inspiration pour vos randonnées.

Toutes les photos présentées ici sont protégées par copyright, mais vous pouvez [me contacter](mailto:hi@tmuguet.me) si vous souhaitez les utiliser.

Les fichiers GPX sont, en théorie, utilisables sur tous types d'appareils/applications. Ils ont été conçus sur mon outil [map2gpx](https://map2gpx.fr/).

### Licences

Ce site a été bâti via [hugo](https://gohugo.io/), avec un thème développé pour l'occasion en open-source, [Hugo Split Gallery theme](https://gitlab.com/tmuguet/hugo-split-gallery).

Ce site utilise:

* [Bootstrap](https://getbootstrap.com/) (licence MIT)
* [fancybox](https://fancyapps.com/fancybox/3/) (licence GPLv3)
* [FileSaver.js](https://github.com/eligrey/FileSaver.js#readme) (licence MIT)
* [Font Awesome](https://fontawesome.com/v4.7/) (licence SIL OFL 1.1)
* [GPX plugin for Leaflet](https://github.com/mpetazzoni/leaflet-gpx) (license BSD 2-clause)
* [Izmir](https://ciar4n.com/izmir/) (licence MIT)
* [jQuery](https://jquery.com/) (licence MIT)
* [JSZip](https://stuk.github.io/jszip/) (licence MIT)
* [Leaflet.awesome-markers](https://github.com/lennardv2/Leaflet.awesome-markers) (licence MIT)
* [Leaflet](https://leafletjs.com/) (licence BSD 2-clause "Simplified")
