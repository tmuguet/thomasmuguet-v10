---
title: "Le long du ruisseau de Vaunoire"
date: 2018-08-15T00:00:00+00:00
images: ["images/IMGP5129.jpg"]
massifs: ["Valbonnais", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Ruisseau"]
---

Balade familiale pour remonter le long du ruissau jusqu'à atteindre un joli plan.
