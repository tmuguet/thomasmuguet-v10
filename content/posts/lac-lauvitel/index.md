---
title: "Lac Lauvitel"
date: 2021-07-11T00:00:00+00:00
images: ["images/IMG_20210301_123612.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: ["Printemps", "Hiver", "Automne", "Été"]
types: ["Marche", "Raquettes"]
seealso: ["posts/lacs-muzelle-lauvitel"]
tags: ["Lac", "Baignade", "Marmottes", "Bouquetins"]
---

Mon lac préféré, facilement accessible, mais un peu victime de son succès.
