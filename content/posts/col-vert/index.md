---
title: "Col Vert"
date: 2016-05-28T00:00:00+00:00
images: ["images/IMGP2304.jpg"]
massifs: "Vercors"
saisons: "Printemps"
types: "Marche"
tags: ["Panorama"]
---

Balade printannière
