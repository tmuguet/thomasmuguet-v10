---
title: "Fort du Mûrier et Fort des Quatre Seigneurs"
date: 2021-05-04T00:00:00+00:00
images: ["images/IMG_20210504_103610.jpg"]
massifs: "Belledonne"
saisons: "Printemps"
types: "Marche"
---

Randonnée tranquille en passant par les forts du Mûrier et des Quatre Seigneurs (tous deux inaccessibles au public) et la montagne du Mulet.
