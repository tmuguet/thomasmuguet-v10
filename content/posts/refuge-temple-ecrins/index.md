---
title: "Refuge de Temple Écrins / Vallon des Étançons"
date: 2019-06-02T00:00:00+00:00
images: ["images/IMGP5981.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: "Été"
types: "Marche"
---

Encore beaucoup de neige en ce début juin, ce qui a contrarié nos plans. Pas de vallon en direction de la Pilatte, pas de vallon du Chardon. Premier jour: montée au refuge, et deuxième jour vallon des Étançons.
