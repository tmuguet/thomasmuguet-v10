---
title: "Col d'Arsine"
date: 2019-06-08T00:00:00+00:00
images: ["images/IMGP6222.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Marmottes"]
---

Balade à un joli spot à marmottes. Trop de neige pour monter au lac d'Arsine ce jour-là.
