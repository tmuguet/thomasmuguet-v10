---
title: "Lac de Brouffier"
date: 2020-05-24T00:00:00+00:00
images: ["images/IMG_20200524_123050.jpg"]
massifs: "Belledonne"
saisons: "Printemps"
types: "Marche"
tags: ["Lac"]
---

Lac facilement accessible avec une jolie boucle
