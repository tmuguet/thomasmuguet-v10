---
title: "Refuge de la Lavey et lac des Rouies"
date: 2015-08-30T00:00:00+00:00
images: ["images/IMGP1107.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: "Été"
types: "Marche"
tags: ["Lac"]
---

Rando tranquille sur deux jours avec nuit au refuge
