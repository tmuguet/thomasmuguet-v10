---
title: "Vallon de Lanchâtra"
date: 2020-05-21T00:00:00+00:00
images: ["images/IMGP2477.jpg"]
massifs: ["Oisans", "Écrins"]
saisons: ["Été", "Printemps"]
types: "Marche"
tags: ["Ruisseau", "Marmottes"]
---

Vallon un peu perdu au pied du glacier éponyme et du glacier de Montagnon
