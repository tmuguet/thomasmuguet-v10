---
title: "Grand Veymont"
date: 2020-06-14T00:00:00+00:00
images: ["images/IMGP7078.jpg"]
massifs: "Vercors"
saisons: ["Automne", "Été"]
types: "Marche"
tags: ["Bouquetins", "Panorama"]
---

Mon premier bouquetin !
