---
title: "Lac de la Muzelle et lac Lauvitel"
date: 2017-07-30T00:00:00+00:00
images: ["images/IMGP3719.jpg"]
massifs: ["Écrins", "Oisans"]
saisons: ["Été"]
types: "Marche"
seealso: ["posts/lac-lauvitel", "posts/lac-muzelle"]
tags: ["Lac"]
---

Randonnée avec nuit en refuge de la Muzelle.
