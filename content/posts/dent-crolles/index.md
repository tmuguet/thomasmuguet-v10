---
title: "Dent de Crolles"
date: 2015-11-11T00:00:00+00:00
images: ["images/IMGP1610.jpg"]
massifs: "Chartreuse"
saisons: "Automne"
types: "Marche"
tags: ["Panorama"]
---

Classique autour de Grenoble
