---
title: "Pas de la Clé"
date: 2016-05-07T00:00:00+00:00
images: ["images/IMGP2153.jpg"]
massifs: "Vercors"
saisons: "Printemps"
types: "Marche"
seealso: "posts/bec-orient-pas-cle-grande-breche"
tags: ["Panorama"]
---

Dernières neiges sur le plateau de Gève
