---
title: "Lac Luitel"
date: 2019-09-08T00:00:00+00:00
images: ["images/IMGP6810.jpg"]
massifs: "Belledonne"
saisons: "Été"
types: "Marche"
tags: ["Tourbière"]
---

Bonne randonnée pour arriver à la tourbière du Luitel (par ailleurs accessible en voiture, sur la montée de Chamrousse).
