---
title: "Plateau d'Emparis"
date: 2023-08-15T00:00:00+00:00
images: ["images/IMGP5015.jpg"]
massifs: ["Cerces", "Oisans", "Écrins"]
saisons: "Été"
types: "Marche"
tags: ["Panorama", "Lac", "Flore"]
---

Balade sur le plateau d'Emparis au lac Lérié et lac Noir, depuis le Chazelet ou Besse.
