---
title: "Lacs du Vénétier"
date: 2017-05-21T00:00:00+00:00
images: ["images/IMGP3485.jpg"]
massifs: "Belledonne"
saisons: "Printemps"
types: "Marche"
tags: ["Lac"]
---

Petite randonnée pour profiter des dernières neiges d'altitude
