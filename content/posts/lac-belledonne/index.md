---
title: "Lac de Belledonne"
date: 2018-07-07T00:00:00+00:00
images: ["images/IMGP4622.jpg"]
massifs: "Belledonne"
saisons: "Été"
types: "Marche"
tags: ["Lac"]
---

Randonnée tranquille, avec en surprise en ce début juillet, de la neige au lac
