---
title: "Roche de Mio"
date: 2021-08-06T00:00:00+00:00
images: ["images/IMG_20210806_120537.jpg"]
massifs: "Vanoise"
saisons: "Été"
types: "Marche"
tags: ["Panorama"]
---

Randonnée à la journée depuis la Plagne à la Roche de Mio en passant par le tunnel des Inversens et le lac des Blanchets.
