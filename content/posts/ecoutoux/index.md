---
title: "Ecoutoux"
date: 2022-01-29T00:00:00+00:00
images: ["images/IMGP0228.jpg"]
massifs: "Chartreuse"
saisons: "Hiver"
types: "Raquettes"
tags: ["Panorama"]
---

Randonnée tranquille à l'Ecoutoux.
