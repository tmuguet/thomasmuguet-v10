---
title: "Cascades de l'Alloix"
date: 2021-06-14T00:00:00+00:00
images: ["images/IMG_20210613_135447_4.jpg"]
massifs: "Chartreuse"
saisons: ["Été", "Printemps"]
types: "Marche"
tags: ["Baignade", "Cascade", "Ruisseau"]
---

Balade familiale -et particulièrement fréquentée- le long du torrent de l'Alloix pour remonter jusqu'à la grande cascade. Parfait pour se rafraichir lors des grosses chaleurs.

À faire au printemps pour avoir une belle cascade, et en été pour se baigner dans l'eau "chaude".
