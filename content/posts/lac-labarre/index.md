---
title: "Lac Labarre"
date: 2019-08-24T00:00:00+00:00
images: ["images/IMGP6782.jpg"]
massifs: ["Écrins", "Valjouffrey"]
saisons: "Été"
types: "Marche"
tags: ["Lac"]
---

Belle randonnée pour atteindre le lac d'altitude Labarre. Monter au col de la Romeïou pour avoir la vue sur le lac.
