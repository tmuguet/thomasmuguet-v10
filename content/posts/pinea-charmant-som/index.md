---
title: "La Pinéa et le Charmant Som"
date: 2021-07-03T00:00:00+00:00
images: ["images/IMGP8337.jpg"]
massifs: "Chartreuse"
saisons: ["Été"]
types: "Marche"
seealso: ["posts/charmant-som"]
tags: ["Panorama"]
---

Grande boucle depuis le col de Porte. Départ à 5h pour voir le lever de soleil à la Pinéa.
